#include <iostream>
#include <thread>
#include <cmath>

int passCommandThread(const std::string command)
{
    return std::system(command.c_str());
}

void playSound(const float latency)
{
    std::string command = "play -n -q ";

#ifdef __linux__
    command += " -t alsa";
#endif

    command += " synth 0.250 sine";

    command += " "+std::to_string(5000*pow(0.998, latency)+150);

    std::thread parallelSound(passCommandThread, command);
    parallelSound.detach();
}
