#include "Line.h"

float getLatency(const Line& pingOutput)
{
    std::string time;
    for (unsigned long i = 0; i<pingOutput.wordsAmount(); i++) {
        if (pingOutput[i].substr(0, 5)=="time=") {
            time = pingOutput[i].substr(5);
            break;
        }
    }

    if (time.length()>0) {
        return std::stof(time);
    }
    else {
        return -1;
    }
}
