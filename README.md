# Ping Ears

This program has `ping` piped to it and outputs sound based on latency.

## Dependencies

You need to have installed `ping` (_obviously_) and [`SoX`](http://sox.sourceforge.net/ "SoX homepage").

Whatever version of ping you're using, the program expects its output to be `X bytes from X.X.X.X thing=value bla=other_bla time=X ms`.
The only things the program pays attention to is the number at the front. If there's no number there, it assumes the packet didn't return. If it does find a number, it looks for `time=X`.

`SoX` has to be callable by the program using the command `play`.
It's pretty simple on Linux, assuming you used your package manager to install it, since then it's all done automatically.
On Windows, though, the SoX executable has to be renamed to `play` for it to work.
Please note that there has been no testing on Windows, so I don't even know if it will compile there.

## Installation
#### Linux
1. Download the repository using `git clone https://www.gitlab.com/matsaki95/ping-ears`.
1. Move into it using `cd ping-ears`.
1. Compile the code using `g++ -O2 -pthread -o ping-ears src/main.cpp`.
1. Your executable is ready.

#### Windows or other
You're on your own.
Good luck!

## Usage
Use ping like you normally do, just pipe it into the program like this: `ping 1.1.1.1 | ping-ears`.

## Other stuff

Determining of what frequency to display is done by using this function (where x is latency and y frequency):

![Formula behind ping-ears](http://www.mediafire.com/convkey/7ff1/ofn432lmuko2gdezg.jpg "f(x)=5000*0.998^x+150")
